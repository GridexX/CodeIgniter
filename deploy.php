<?php
namespace Deployer;

require 'recipe/codeigniter.php';

// Config

set('repository', '');

add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

// Hosts

host('51.158.121.252')
    ->set('remote_user', 'root')
    ->set('deploy_path', '~/deploy');

// Hooks

after('deploy:failed', 'deploy:unlock');

